package io.FallenDev;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.BlockIterator;


/**
 * Created by FallenYouth
 **/

public class Main extends JavaPlugin implements Listener {

    public void onEnable() {

        Bukkit.getServer().getPluginManager().registerEvents(this, this);

    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Arrow) {
            if (event.getEntity().getShooter() instanceof Player){
                Entity entity = event.getEntity();
                BlockIterator iterator = new BlockIterator(entity.getWorld(), entity.getLocation().toVector(), entity.getVelocity().normalize(), 0, 4);
                Block hitBlock = null;

                while(iterator.hasNext()) {
                    hitBlock = iterator.next();
                    if(hitBlock.getTypeId()!=0) {
                        break;
                    }
                }

                LivingEntity liv = event.getEntity().getShooter();
                Player p = (Player)liv;
                    if(p.getInventory().getItemInHand().getType().equals(Material.BOW)){
                        int tpX = (int) hitBlock.getLocation().getX();
                        int tpY = (int) hitBlock.getLocation().getY();
                        int tpZ = (int) hitBlock.getLocation().getZ();
                        int tpyaw = (int) p.getLocation().getYaw();
                        int tppitch = (int) p.getLocation().getPitch();
                        Location loc = new Location(p.getWorld(),tpX,tpY,tpZ,tpyaw,tppitch);
                        p.teleport(loc);
                        p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 10000);
                        p.playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 10000);
                        p.playEffect(p.getLocation(), Effect.SMOKE, 10000);
                    }

                }
            }
        }
    }